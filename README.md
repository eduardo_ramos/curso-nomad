Curso introdutório ao Hashicorp Nomad
===

<<<<<<< HEAD
Table Of Contents
===

- [1. Introdução](#1 - Introdução)
  * [1.1 Proposta do curso](#11-proposta)
  * [1.2 Escopo](#12-escopo)
  * [1.3 Acrônimos](#13-acrônimos)
  * [1.4 Referências](#14-referências)
    + [1.4.1 Referências internas](#141-internal-references)
    + [1.4.2 Referências externas](#142-external-references)
  * [1.5 Revisão do capítulo](#15-Revisão do capítulo)
=======
Estou escrevendo aos poucos o curso a partir deste link abaixo.  
[Curso Nomad](https://gitlab.com/eduardo_ramos/curso-nomad/-/wikis/curso-nomad)
>>>>>>> b651df0e42f3799673e769f27fae0de9d105af82


1 - Introdução
===

Inicialmente gostaria de me apresentar. Me chamo Eduardo Ramos, 47, casado e pai de dois filhos. Sou pós-graduado em sistemas de Cloud Computing pela Universidade Descomplica, sou Analista DevOps e entusiasta GNU/Linux há mais de vinte anos.

O objetivo deste curso é proporcionar ao colega uma experiência tranquila na utilização Hashicorp Nomad. E por que usar Nomad?

Hashicorp Nomad é uma ferramenta de orquestração de contêineres e aplicativos que pode ser usada por várias razões, incluindo:

1. Alta escalabilidade: Nomad permite escalar facilmente aplicativos e serviços em todo o cluster, o que é útil em cenários de alta demanda.

2. Flexibilidade: Nomad suporta vários tipos de aplicativos, incluindo contêineres, aplicativos de missão crítica, aplicativos legados e aplicativos de linha de comando.

3. Gerenciamento de recursos: Nomad oferece uma solução eficiente para gerenciar recursos como CPU, memória e espaço em disco para aplicativos e serviços em execução.

4. Alta disponibilidade: Nomad é projetado para ser altamente disponível, o que significa que os aplicativos e serviços continuam a ser executados mesmo se houver falhas no cluster.

5. Integração com outras ferramentas: Nomad pode ser facilmente integrado a outras ferramentas da stack Hashicorp, como Vault e Consul, para melhorar a segurança e gerenciamento de configurações.

Em resumo, Hashicorp Nomad é uma solução de orquestração de aplicativos e contêineres flexível, escalável e disponível que oferece uma ampla gama de recursos para gerenciar aplicativos e serviços em um cluster.

O que você não observará neste curso é a equiparação do Nomad com o Kubernetes. É preciso vir com espírito livre e despido de julgamentos pois um nada tem a ver com o outro (A Hashicorp deixa, inclusive, isso bem claro!). Dito isso, teremos uma boa experiência nesta ferramenta que para mim foi um divisor de águas sobretudo na facilidade de uso.

Agradecimentos
===

Antes de mais nada, quero aqui deixar meus agradecimentos ao Luiz Aoqui que pacientemente responde minhas mensagens e dizer que ele foi fundamental na minha virada de chave para o Nomad. Muito obrigado, Luiz!

À minha esposa Patrícia e filhos que estão sempre comigo e me dão amor, carinho e me dão sentido para levantar todo dia com propósito e entregar o melhor de mim. Amo vocês!



