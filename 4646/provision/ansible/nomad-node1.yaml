---
- hosts: all
  become: yes
  become_user: root
  become_method: sudo
  tasks:
    - name: Garantindo /etc/hosts
      lineinfile:
        path: /etc/hosts
        line: "{{ item }}"
      with_items:
        - 172.35.1.100 nomad-master
        - 172.35.1.101 nomad-node1
        - 172.35.1.102 nomad-node2
        - 172.35.1.103 registry

    - name: Adiciona usuario suporte
      user:
        name: suporte
        shell: /bin/bash
        password : $1$QbUARykG$p2nthVG8AkDvabKPHwboa1

    - name: Instala pacotes que permitem que o apt seja usado sobre HTTPS
      apt:
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
        - apt-transport-https
        - ca-certificates
        - curl
        - gnupg-agent
        - software-properties-common
        - nfs-common
        - git
        - vim
        - wget 
        - gpg 
        - coreutils

    - name: Clona repositório com os arquivos do curso para a HOME do usuario suporte
      git:
        repo: 'https://gitlab.com/eduardo_ramos/curso-nomad.git'
        dest: /home/suporte/4646

    - name: Cria o diretório .ssh
      file:
        path: /home/suporte/.ssh
        state: directory
        owner: suporte
        group: suporte
        mode: '0755'

    - name: Cria o diretório app
      file:
        path: /opt/app
        state: directory
        owner: root
        group: root
        mode: '0755'

    - name: Monta compartilhamento NFS
      mount:
        src: 172.35.1.103:/opt/app
        path: /opt/app
        opts: rw,sync,hard
        state: mounted
        fstype: nfs

    - name: Criando o arquivo id_rsa
      copy:
        src: files/id_rsa
        dest: /home/suporte/.ssh/id_rsa
        owner: suporte
        group: suporte
        mode: '0600'

    - name: Criando o arquivo id_rsa.pub
      copy:
        src: files/id_rsa.pub
        dest: /home/suporte/.ssh/id_rsa.pub
        owner: suporte
        group: suporte
        mode: '0644'

    - name: Cria o diretório data
      file:
        path: /home/suporte/data
        state: directory
        owner: suporte
        group: suporte
        mode: '0755'

    - name: Criando o arquivo authorized_keys
      copy:
        src: files/authorized_keys
        dest: /home/suporte/.ssh/authorized_keys
        owner: suporte
        group: suporte
        mode: '0644'

    - name: Instala pacotes que permitem que o apt seja usado sobre HTTPS
      apt:
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
        - apt-transport-https
        - ca-certificates
        - curl
        - gnupg-agent
        - software-properties-common
        - nfs-common
        - vim

    - name: Adiciona uma chave de assinatura apt para o Docker
      apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present

    - name: Adiciona repositorio apt para versao estavel
      apt_repository:
        repo: deb [arch=amd64] https://download.docker.com/linux/ubuntu xenial stable
        state: present

    - name: Instala o Docker e suas dependencias
      apt:
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
        - docker-ce
        - docker-ce-cli
        - containerd.io
      notify:
        - docker status

    - name: Adiciona o usuario suporte no grupo docker
      user:
        name: suporte
        group: docker

    - name: Cria o diretório registry:5000
      file:
        path: /etc/docker/certs.d/registry:5000
        state: directory
        owner: root
        group: root
        mode: '0755'

    - name: Criando o arquivo ca.crt
      copy:
        src: files/ca.crt
        dest: /etc/docker/certs.d/registry:5000
        owner: suporte
        group: suporte
        mode: '0644'

    - name: Adiciona uma chave de assinatura apt para o Nomad e Consul
      apt_key:
        url: https://apt.releases.hashicorp.com/gpg
        state: present

    - name: Adiciona repositorio apt para versao estável
      ansible.builtin.apt_repository:
        repo: "deb https://apt.releases.hashicorp.com focal main"
        state: present

    - name: Instala o Nomad e Consul
      apt:
        name: "{{ packages }}"
        state: present
        update_cache: yes
      vars:
        packages:
        - nomad
        - consul

    - name: Criando o arquivo nomad.hcl
      copy:
        src: files/nomad/client/nomad-node1.hcl
        dest: /etc/nomad.d/nomad.hcl
        owner: nomad
        group: nomad
        mode: '0644'

    - name: Criando o arquivo consul.hcl
      copy:
        src: files/consul/client/consul.hcl
        dest: /etc/consul.d/consul.hcl
        owner: consul
        group: consul
        mode: '0644'

    - name: Criando o arquivo server.hcl
      copy:
        src: files/consul/client/server102.hcl
        dest: /etc/consul.d/server.hcl
        owner: consul
        group: consul
        mode: '0644'

    - name: Copy systemd init file
      copy:
        src: files/consul/client/consul.service
        dest: /etc/systemd/system/consul.service
        owner: consul
        group: consul
        mode: '0644'

    - name: Reinicia o Consul
      service:
        name: consul
        daemon_reload: yes
        state: restarted

    - name: Reinicia o Nomad
      service:
        name: nomad
        daemon_reload: yes
        state: restarted

    - name: Adiciona o usuario suporte no grupo docker
      user:
        name: suporte
        group: docker

    - name: Criando o arquivo daemon.json
      copy:
        src: files/daemon.json
        dest: /etc/docker/
        owner: root
        group: root
        mode: '0644'

    - name: Cria o diretorio docker.service.d
      file:
        path: /etc/systemd/system/docker.service.d
        state: directory
        owner: root
        group: root
        mode: '0755'

    - name: Reinicia o Docker
      service:
        name: docker
        daemon_reload: yes
        state: restarted

  handlers:
    - name: docker status
      service: name=docker state=started
