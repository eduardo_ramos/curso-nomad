log_level = "INFO"
datacenter = "dc1"
name="nomad-master"

plugin_dir = "/var/nomad/plugins"

advertise {
  http = "{{GetInterfaceIP \"eth1\"}}"
  rpc  = "{{GetInterfaceIP \"eth1\"}}"
  serf = "{{GetInterfaceIP \"eth1\"}}"
}

client {
        enabled = true
        network_interface = "eth1"
}

client {
  enabled = true
  host_volume "app" {
    path      = "/opt/app"
    read_only = false
  }
}

ports {
  http = 4646
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

telemetry {
  collection_interval = "10s"
}

plugin "docker" {
  config {
    auth {
      config = "/root/.docker/config.json"
    }
    allow_privileged = true
    volumes {
      enabled      = true
    }
  }
}

