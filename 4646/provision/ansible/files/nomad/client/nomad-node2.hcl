log_level = "INFO"
datacenter = "dc1"
data_dir = "/opt/nomad"
name="nomad-node2"

advertise {
  http = "{{GetInterfaceIP \"eth1\"}}"
  rpc  = "{{GetInterfaceIP \"eth1\"}}"
  serf = "{{GetInterfaceIP \"eth1\"}}"
}

client {
        enabled = true
        network_interface = "eth1"
}

consul {
  address             = "127.0.0.1:8500"
  server_service_name = "nomad"
  client_service_name = "nomad-node2"
  auto_advertise      = true
  server_auto_join    = true
  client_auto_join     = true
}

plugin_dir = "/var/nomad/plugins"

client {
  enabled = true
  servers = ["172.35.1.100:4647"]
  network_interface = "eth1"

  host_volume "app" {
    path      = "/opt/app"
    read_only = false
  }
}

ports {
  http = 4646
}

plugin "raw_exec" {
  config {
    enabled = true
  }
}

telemetry {
  collection_interval = "10s"
}

plugin "docker" {
  config {
    auth {
      config = "/root/.docker/config.json"
    }
    allow_privileged = true
    volumes {
      enabled      = true
    }
  }
}

